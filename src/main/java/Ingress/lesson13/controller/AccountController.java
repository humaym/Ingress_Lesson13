package Ingress.lesson13.controller;


import Ingress.lesson13.model.Account;
import Ingress.lesson13.repository.AccountRepository;
import Ingress.lesson13.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;
    private final AccountRepository accountRepository;

    @GetMapping("/{id}")
    public Account getAccount(@PathVariable Long id){
        return accountRepository.findById(id).get();

    }
}
