package Ingress.lesson13.service;

import Ingress.lesson13.model.Account;
import Ingress.lesson13.model.PhoneNumber;
import Ingress.lesson13.repository.AccountRepository;
import Ingress.lesson13.repository.PhoneNumberRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.engine.spi.SessionDelegatorBaseImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@RequiredArgsConstructor
@Slf4j
public class AccountService {

    private final AccountRepository accountRepository;

    private final PhoneNumberRepository phoneNumberRepository;
    private final EntityManagerFactory emf;

    public static Map<Long, Account> cache = new HashMap<>();

    public void assignPhoneNumbersToAccount(Long accountId, List<Long> phoneNumberIds) {
        Account account = accountRepository.findById(accountId).orElseThrow();
        phoneNumberIds.forEach(i -> phoneNumberRepository.findById(i).ifPresent(phoneNumber -> {
            phoneNumber.setAccount(account);
            account.getPhoneNumberList().add(phoneNumber);
        }));
        accountRepository.save(account);
    }

    public void assignPhoneNumbersToAccountWithReference(Long accountId, List<Long> phoneNumberIds) {

        Account account = accountRepository.getOne(accountId);
        phoneNumberIds.forEach(i -> {
            PhoneNumber phoneNumberReference = phoneNumberRepository.getReferenceById(i);
            phoneNumberReference.setAccount(account);
            account.getPhoneNumberList().add(phoneNumberReference);
        });
        accountRepository.save(account);
    }

//    public void assignPhoneNumbersToAccountWithReference(Long accountId, List<Long> phoneNumberIds) {
//        Account account = accountRepository.getOne(accountId);
//        for (Long phoneNumberId : phoneNumberIds) {
//            PhoneNumber phoneNumber = phoneNumberRepository.getReferenceById(phoneNumberId);
//            phoneNumber.setAccount(account);
//            account.getPhoneNumberList().add(phoneNumber);
//        }
//        accountRepository.save(account);
//    }


    public Account getByCache(Long id) {
        log.info("Method with cache started");
        Account account = cache.get(id);
        if (account == null) {
            log.info("Account in cache not found. Getting from DB");
            Account account1 = accountRepository.findById(id).orElseThrow();
            Account put = cache.put(id, account1);
            return put;
        }
        log.info("Account in cache found. Returning");
        return account;
    }

    public void transferProxy(Long frmId, Long toId, Double amount) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            Account from = em.find(Account.class, frmId);
            Account to = em.find(Account.class, toId);
            transfer(from, to, amount);
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
        } finally {
            em.getTransaction().commit();
            em.close();
        }
    }


    public void transfer(Account from, Account to, Double amount) {
        if (from.getBalance() < amount) {
            throw new RuntimeException("Balance not enough");
        }
        from.setBalance(from.getBalance() - amount);
        to.setBalance(to.getBalance() + amount);
        accountRepository.save(from);
        accountRepository.save(to);
    }

}

