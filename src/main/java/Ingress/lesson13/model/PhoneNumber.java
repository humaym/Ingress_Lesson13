package Ingress.lesson13.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;
import lombok.*;
import lombok.experimental.FieldDefaults;
import lombok.experimental.FieldNameConstants;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@FieldNameConstants
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE) //Provide cache strategy.
public class PhoneNumber {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String phone;

    @ManyToOne
    @ToString.Exclude
    @JsonIgnore
    Account account;


    @CachePut(value = "phoneNumber", key = "#result.id")
    public PhoneNumber saveOrUpdate() {
        // Save or update the entity
        return this;
    }

    @CacheEvict(value = "phoneNumber", key = "#this.id")
    public void delete() {
        // Delete the entity
    }
}


