package Ingress.lesson13;

import Ingress.lesson13.model.Account;
import Ingress.lesson13.model.PhoneNumber;
import Ingress.lesson13.repository.AccountRepository;
import Ingress.lesson13.service.AccountService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class Lesson13Application implements CommandLineRunner {

    private final AccountRepository accountRepository;
    private final AccountService accountService;
    private final EntityManagerFactory emf;

    public static void main(String[] args) {
        SpringApplication.run(Lesson13Application.class, args);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void run(String... args) throws Exception {


//        log.info("findById method is running");
//        accountService.assignPhoneNumbersToAccount(13L, List.of(1L, 2L));
//        log.info("getReferenceById is running");
//        accountService.assignPhoneNumbersToAccountWithReference(13L, List.of(1L, 2L));
//
//        //        task1
//        EntityManager em = emf.createEntityManager();
//        em.getTransaction().begin();
//        Account account = em.find(Account.class, 3L);
//        account.setName("Humay4");
//        em.flush();
//        em.detach(account);
//        Account mergedAccount = em.merge(account);
//        mergedAccount.setName("Humay");
//        em.getTransaction().commit();
//        em.close();


        //manual cache
/*        Account account = accountService.getByCache(3L);
        Account account1 = accountService.getByCache(3L);*/


//        log.info("#1 getting account with id 3");
//        Account account = accountRepository.findById(3L).get();
//        log.info("Account is {}", account);
//        log.info("#2 getting account with id 3");
//        Account account1 = accountRepository.findById(3L).get();
//        log.info("Account is {}", account1);


//        log.info("#1 getting account with id 3");
//        Account account2 = accountRepository.findByName("Humay5").get();
//        log.info("Account is {}", account2);
//        log.info("#2 getting account with id 3");
//        Account account3 = accountRepository.findByName("Humay5").get();
//        log.info("Account is {}", account3);


//        log.info("#1 getting all accounts");
//        var account2 = accountRepository.findAll();
//        log.info("Account is {}", account2);
//        log.info("#2 getting all accounts");
//        var account3 = accountRepository.findAll();
//        log.info("Account is {}", account3);


//        SessionFactory sf = emf.unwrap(SessionFactory.class);
//        Session session = sf.openSession();
//        session.beginTransaction();
//        log.info("#1 getting account with id 3");
//        var account = session.get(Account.class, 3L);
//        log.info("Account is {}", account);
//        log.info("Waiting 10 sec");
//        Thread.sleep(10000);
//        log.info("#2 getting account with id 3");
//        session.evict(account);
//        var account1 = session.get(Account.class, 3L);
//        log.info("Account is {}", account1);
//        session.getTransaction().commit();
//        session.close();


//        Account account = accountRepository.findById(1L).get();
//
//        PhoneNumber a = new PhoneNumber();
//        a.setPhone("12345474");
//        a.setAccount(account);
//
//        PhoneNumber b = new PhoneNumber();
//        b.setPhone("347584537");
//        b.setAccount(account);
//
//        account.getPhoneNumberList().add(a);
//        account.getPhoneNumberList().add(b);
//        accountRepository.save(account);
//
//        Account account2 = accountRepository.findById(2L).get();
//
//        PhoneNumber a2 = new PhoneNumber();
//        a2.setPhone("12345474");
//        a2.setAccount(account2);
//
//        PhoneNumber b2 = new PhoneNumber();
//        b2.setPhone("347584537");
//        b2.setAccount(account2);
//
//        account2.getPhoneNumberList().add(a2);
//        account2.getPhoneNumberList().add(b2);
//        accountRepository.save(account2);


//		Account account1=new Account();
//		account1.setName("Kanan");
//		account1.setBalance(300.0);
//
//		Account account2=new Account();
//		account2.setName("Nicat");
//		account2.setBalance(500.0);
//		accountRepository.save(account1);
//		accountRepository.save(account2);
//        Account from = accountRepository.findById(3L).get();
//        Account to = accountRepository.findById(4L).get();
//
//        accountService.transfer(from, to, 30.0);



    }
}
