package Ingress.lesson13.repository;

import Ingress.lesson13.model.Account;
import Ingress.lesson13.model.PhoneNumber;
import jakarta.persistence.Entity;
import org.hibernate.boot.model.source.spi.AttributePath;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface PhoneNumberRepository extends JpaRepository<PhoneNumber, Long> {

}
