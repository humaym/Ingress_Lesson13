package referenceTypes;

public class Car {

    public void hello() {
        System.out.println("Hello");
    }

    @Override
    public void finalize() {
        System.out.println("Car is deleted");
    }

}