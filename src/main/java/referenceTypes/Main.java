package referenceTypes;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.text.ParseException;

public class Main {
    public static void main(String[] args) throws ParseException,InterruptedException {
        Car car=new Car();
        car=null;
        System.gc();
        Thread.sleep(3000);

        WeakReference<Car> weakReference=new WeakReference<>(new Car());
        weakReference.get().hello();
        System.gc();
        Thread.sleep(3000);

        SoftReference<Car> softReference=new SoftReference<>(new Car());
        softReference.get().hello();

        ReferenceQueue<Car> referenceQueue=new ReferenceQueue<>();
        PhantomReference<Car> phantomReference=new PhantomReference<>(new Car(),referenceQueue);

    }

}
